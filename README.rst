=====================
MCEP Alpha Calculator
=====================

Mel-Cepstral analysis of speech data, such as with the help of the
SPTK toolkit, requires specifying an all-pass constant (alpha) that
determines how the frequency axis is warped. This constant depends on
the sampling frequency of the speech data at hand.

This project provides a tool that allows you to calculate the
appropriate alpha paramater for a given sampling frequency.

Installation
============

This project is written in Go (www.golang.org). Having installed the
"go" compilation tools, please run

$ go build

in this directory to build. 

You could also download precompiled binaries from the "Downloads" page on Bitbucket.

Usage
=====

$ ./mcep_alpha_calc -f 16000

Technical Details
=================

The Mel-Scale is estimated using the formula noted in Fant (1968)

  m = (1000/log(2)) * log(1 + f/1000)


This curve is estimated at equally spaced points between
[0,samp_freq/2] and normalized.  Given an alpha value, the phase
response of an all-pass filter is calculated for each of these points
using the equation:

  \~w = \arctan{\frac{(1-\alpha^2) \sin(\omega)}{(1+\alpha^2) \cos(\omega) - 2\alpha}}

We brute-force over different values of alpha between 0 and 1, and
find the alpha where the root-mean-squared distance between the warped
frequency and the mel scale is minimum.

Sample Output
=============

.. table:: Generated Alpha for commonly used Frequencies
  
  =========     =====
  Frequency     Alpha
  =========     =====
  8000  Hz      0.312
  11025 Hz      0.357
  16000 Hz      0.410
  22050 Hz      0.455
  32000 Hz      0.504
  44100 Hz      0.544
  48000 Hz      0.554
  =========     =====
