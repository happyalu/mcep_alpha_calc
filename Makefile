.phony: all build clean crosscompile

all: build

build:
	go build

crosscompile:
	mkdir -p bin_all
	@PLATFORMS="darwin/386 darwin/amd64 freebsd/386 freebsd/amd64 freebsd/arm linux/386 linux/amd64 linux/arm windows/386 windows/amd64"; \
		echo $$PLATFORMS; \
		for PLATFORM in $$PLATFORMS; do \
			echo $$PLATFORM; \
			export GOOS=$${PLATFORM%/*}; \
			export GOARCH=$${PLATFORM#*/}; \
			export GOEXT=""; \
			if [ "$$GOOS" = "windows" ] ; then export GOEXT=".exe"; fi; \
			CMD="go build -o bin_all/mcep_alpha_calc.$$GOOS.$$GOARCH$$GOEXT"; \
			$$CMD; \
		done

clean:
	rm -f mcep_alpha_calc
	rm -rf bin_all
